<?php
/* PlumTree functions and definitions */
add_action( 'wp_enqueue_scripts', 'handystore_child_enqueue_styles' );



function handystore_child_enqueue_styles() {
 
    $parent_style = 'parent-style';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style, 'plumtree-woo-styles' ),
        wp_get_theme()->get('Version')
    );
}

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Page Slider',
    'before_widget' => '<div class = "widgetizedArea">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);
function pt_store_title(){
  if ( (is_shop() || is_product_category() || is_product_tag()) && !is_front_page() ) {
     ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="page-title">
        <?php echo single_cat_title() ?>
      </div>
    </div> 
    <?php
  }
}
 add_action( 'init', 'pt_store_title', 20 );

 function pt_custom_breadcrumbs() {
  return array(
    'delimiter' => '<span> &#47; </span>',
    'wrap_before' => '<div class="col-md-12 col-sm-12 col-xs-12"><nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
    'wrap_after' => '</nav></div>',
    'before' => '',
    'after' => '',
    'home' => _x( 'Home', 'breadcrumb', 'handystore' ),
  );
}

add_action( 'init', 'pt_custom_breadcrumbs', 20 );

// Adding view all Link
add_action( 'woocommerce_before_shop_loop', 'pt_view_all_link', 25 );
if ( ! function_exists( 'pt_view_all_link' ) ) {
  function pt_view_all_link(){
    global $wp_query;
    $paged    = max( 1, $wp_query->get( 'paged' ) );
    $per_page = $wp_query->get( 'posts_per_page' );
    $total    = $wp_query->found_posts;
    $first    = ( $per_page * $paged ) - $per_page + 1;
    $last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );

    /* Get vendor store pages */
    $vendor_shop = '';
    if ( class_exists('WCV_Vendors') ) {
      $vendor_shop = urldecode( get_query_var( 'vendor_shop' ) );
    }

    if ( !is_search() && $wp_query->max_num_pages > 1 && $vendor_shop == '' ) { ?>
      <a rel="nofollow" class="view-all" href="?showall=1"><?php _e('Prikaži više', 'handystore'); ?></a>
    <?php }
    if( isset( $_GET['showall'] ) ){
      $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) ); ?>
        <a rel="nofollow" class="view-all" href="<?php echo esc_url($shop_page_url); ?>"><?php _e('Prikaži manje', 'handystore'); ?></a>
    <?php }
  }
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

// Remove the product rating display on product loops
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );


// remove meta data
add_action('init', 'removeMetaData', 4);

function removeMetaData(){
  remove_action('woocommerce_after_single_product_summary', 'woocommerce_template_single_meta', 4);
}

/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
  global $product; 
	$tabs['description']['title'] = __( 'Opis proizvoda' );		// Rename the description tab
	$tabs['reviews']['title'] = __( 'Ocene ('.$product->get_review_count().')' );				// Rename the reviews tab
	return $tabs;
}

function pt_custom_product_tabs( $tabs ) {
  global $product;
  $product_content = $product->get_description();
  if ($product_content && $product_content!=='') {
    $tabs['description']['priority'] = 10;
  } else {
    unset( $tabs['description'] );
  }
  if( $product->has_attributes() || $product->has_dimensions() || $product->has_weight() ) {
    $tabs['additional_information']['title'] = __( 'Specifikacije', 'handystore' );
    $tabs['additional_information']['priority'] = 20;
  } else {
    unset( $tabs['additional_information'] );
  }
  return $tabs;
}

// Change the description tab heading to product name
add_filter( 'woocommerce_product_description_heading', 'wc_change_product_description_tab_heading', 10, 1 );
function wc_change_product_description_tab_heading( $title ) {
	return '';
}

add_filter( 'woocommerce_product_additional_information_heading', 'wc_change_product_spec_tab_heading', 10, 1 );
function wc_change_product_spec_tab_heading( $title ) {
	return 'Specifikacije';
}

add_filter( 'woocommerce_reviews_title', 'wc_change_product_reviews_tab_heading', 10, 1 );
function wc_change_product_reviews_tab_heading( $title ) {
  global $product;
  $string = '';
  if($product->get_review_count() > 1){
    $string = ' ocena';
  }else{
    $string = ' ocenu';
  }
  return $product->get_title().' ima '.$product->get_review_count().$string;
}

add_filter( 'gettext', 'translated', 999, 3 );
//add_filter('ngettext', 'no_reviews_heading', 20, 3);
function translated( $translated, $text, $domain ) {
 
	if( is_product() && $translated == 'Reviews' && $domain == 'woocommerce' ) {
		$translated = 'Ocene';
  }
  if($translated == 'No products in the cart.' && $domain == 'woocommerce' ) {
    $translated = 'Nema proizvoda u korpi.';
  }
  
  if($translated == 'Subtotal' && $domain == 'woocommerce' ) {
    $translated = 'Zbir';
  }
  if($translated == 'Total' && $domain == 'woocommerce' ) {
    $translated = 'Ukupno';
  }
  if($translated == 'Cart totals' && $domain == 'woocommerce' ) {
    $translated = 'Ukupno iz korpe';
  }
  if($translated == 'View cart' && $domain == 'woocommerce' ) {
    $translated = 'Pogledaj korpu';
  }
  if($translated == 'Checkout' && $domain == 'woocommerce' ) {
    $translated = 'Proveri proizvode';
  }
  if($translated == 'Sold By:' && $domain == 'woocommerce'){
    $translated = "Prodaje:";
  }
  if($translated == 'Price:' && $domain == 'woocommerce'){
    $translated = "Cena:";
  }
  if($translated == 'Filter' && $domain == 'woocommerce'){
    $translated = "Primeni";
  }
	return $translated;
 
}

function pt_comment_form() {

  $commenter = wp_get_current_commenter();
  $req = get_option( 'require_name_email' );
  $aria_req = ( $req ? " aria-required='true'" : '' );
  $user = wp_get_current_user();
  $user_identity = $user->exists() ? $user->display_name : '';

  $custom_args = array(
      'id_form'           => 'commentform',
      'id_submit'         => 'submit',
      'title_reply'       => __( 'Ostavite Vaš komentar', 'handystore' ),
      'title_reply_to'    => __( 'Ostavite Vaš komentar za %s', 'handystore' ),
      'cancel_reply_link' => __( 'Otkaži komentar', 'handystore' ),
      'label_submit'      => __( 'Pošalji', 'handystore' ),

      'comment_field' =>  '<p class="comment-form-comment">
                           <label for="comment">'.__( 'Komentar', 'handystore' ).'</label>
                           <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" aria-describedby="form-allowed-tags" placeholder="'.__('Komentar:', 'handystore').'"></textarea>
                           </p>',

      'must_log_in' => '<p class="must-log-in">'.
                        sprintf( __( 'Morate biti <a href="%s">ulogovani</a> da biste postavili komentar.', 'handystore' ), wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ) ).
                       '</p>',

      'logged_in_as' => '<p class="logged-in-as">'.
                         sprintf( __( 'Ulogovani ste kao: <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Izlogujte se?</a>', 'handystore' ),
                          admin_url( 'profile.php' ),
                          $user_identity,
                          wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ).
                        '</p>',

      'comment_notes_before' => false,

      'comment_notes_after' => false,

      'fields' => apply_filters( 'comment_form_default_fields', array(
          'author' =>
                      '<p class="comment-form-author">
                      <label for="author">'. __( 'Ime', 'handystore' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label>
                      <input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" aria-required="true" placeholder="' . ( $req ? __( 'Ime (obavezno):', 'handystore' ) : __( 'Ime:', 'handystore' ) ) . '" />
                      </p>',

          'email' =>
                      '<p class="comment-form-email">
                      <label for="email">'. __( 'E-mail', 'handystore' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label>
                      <input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" aria-required="true" aria-describedby="email-notes" placeholder="' . ( $req ? __( 'E-mail (obavezno):', 'handystore' ) : __( 'E-mail:', 'handystore' ) ) . '" />
                      </p>',

          'url' =>
                      '<p class="comment-form-url">
                      <label for="url">'. __( 'Website', 'handystore' ) . '</label>
                      <input id="url" name="url" type="text" value="' . esc_url( $commenter['comment_author_url'] ) . '" placeholder="' . __( 'Website:', 'handystore' ) . '" />
                      </p>',
      )),
  );
  comment_form( $custom_args );
}

// To change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Naruči', 'woocommerce' ); 
}

add_filter('wc_add_to_cart_message', 'handler_function_name', 10, 2);
function handler_function_name($message, $product) {
    return "Vaš proizvod '".get_the_title($product)."' je u korpi.";
}

add_filter('woocommerce_get_availability', 'availability_filter_func');
function availability_filter_func($availability){
  $availability['availability'] = str_ireplace('Out of stock', 'Nema na stanju', $availability['availability']);
  $availability['availability'] = str_ireplace('Available on backorder', 'Dostupno u narudžbini', $availability['availability']);
  if(stripos($availability['availability'], 'in stock')){
    $availability['availability'] = str_ireplace('in stock', 'na stanju', $availability['availability']);
  }
  
  return $availability;
}

add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product)
{
    return '<span class="onsale">RASPRODAJA</span>';
}

add_filter( 'woocommerce_get_script_data', 'change_alert_text', 10, 2 );
function change_alert_text( $params, $handle ) {
    if ( $handle === 'wc-add-to-cart-variation' )
        $params['i18n_no_matching_variations_text'] = __( 'Izvinite, za Vašu kombinaciju opcija ne postoji proizvod. Pokušajte opet.', 'woocommerce' );
        $params['i18n_make_a_selection_text'] = __( 'Molimo Vas da izaberete opcije proizvoda pre nego što ga dodate u korpu.', 'woocommerce' );
        $params['i18n_unavailable_text'] = __( 'Izvinite, ovaj proizvod nije dostupan. Molimo Vas da izaberete drugačiju kombinaciju.', 'woocommerce' );
    return $params;
}

function my_dropdown_variation_attribute_options_html($html, $args){
  $html = str_replace('Choose an option', 'Izaberite opciju', $html);
  return $html;
}
add_filter('woocommerce_dropdown_variation_attribute_options_html', 'my_dropdown_variation_attribute_options_html', 10, 2);

add_action( 'woocommerce_reset_variations_link' , 'sd_change_clear_text', 15 );
function sd_change_clear_text() {
   echo '<a class="reset_variations" href="#">' . esc_html__( 'Obriši', 'woocommerce' ) . '</a>';
 
}
function woocommerce_button_proceed_to_checkout() {
  $checkout_url = WC()->cart->get_checkout_url(); ?>
  <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="checkout-button button alt wc-forward">
  <?php esc_html_e( 'Potvrdi narudžbinu', 'woocommerce' ); ?>
  </a>
  <?php
 }

 function changeFilters(){
   return esc_html__('Podelite:', 'handystore');
 }

 add_filter( 'handy_share_btns_welcome_text', 'changeFilters' );


 function pt_entry_post_views() {
  global $post;
  $views = get_post_meta ($post->ID,'views',true);
  if ($views) {
      echo '<div class="post-views"><span>'.__('Pregledi: ', 'handystore').'</span><i class="fa fa-eye"></i>('.$views.')</div>';
  } else { echo '<div class="post-views"><span>'.__('Pregledi: ', 'handystore').'</span><i class="fa fa-eye"></i>(0)</div>'; }
}

class pt_comments_walker extends Walker_Comment {
  var $tree_type = 'comment';
  var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );

  // wrapper for child comments list
  function start_lvl( &$output, $depth = 0, $args = array() ) {
      $GLOBALS['comment_depth'] = $depth + 1; ?>
      <div class="child-comments comments-list">
  <?php }

  // closing wrapper for child comments list
  function end_lvl( &$output, $depth = 0, $args = array() ) {
      $GLOBALS['comment_depth'] = $depth + 1; ?>
    </div>
  <?php }

  // HTML for comment template
  function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
      $depth++;
      $GLOBALS['comment_depth'] = $depth;
      $GLOBALS['comment'] = $comment;
      $parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' );
      if ( 'article' == $args['style'] ) {
          $add_below = 'comment';
      } else {
          $add_below = 'comment';
      } ?>

  <article <?php comment_class(empty( $args['has_children'] ) ? '' :'parent') ?> id="comment-<?php comment_ID() ?>" itemprop="comment" itemscope="itemscope" itemtype="http://schema.org/UserComments">
      <figure class="gravatar"><?php echo get_avatar( $comment, 70 ); ?></figure>

      <div class="comment-meta" role="complementary">
          <h2 class="comment-author" itemprop="creator" itemscope="itemscope" itemtype="http://schema.org/Person">
              <?php _e('Postavio/la ', 'handystore'); ?>
              <?php if (get_comment_author_url() != '') { ?>
                  <a class="comment-author-link" href="<?php esc_url(comment_author_url()); ?>" itemprop="url"><span itemprop="name"><?php comment_author(); ?></span></a>
              <?php } else { ?>
                  <span class="author" itemprop="name"><?php comment_author(); ?></span>
              <?php } ?>
          </h2>
          <?php _e(' na datum ', 'handystore'); ?>
          <time class="comment-meta-time" datetime="<?php comment_date() ?>T<?php comment_time() ?>" itemprop="commentTime"><?php comment_date() ?><?php _e(', u ', 'handystore');?><a href="#comment-<?php comment_ID() ?>" itemprop="url"><?php comment_time() ?></a></time>
          <?php edit_comment_link(esc_html__('Promeni', 'handystore'),'',''); ?>
          <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>

      <?php if ($comment->comment_approved == '0') : ?>
          <p class="comment-meta-item"><?php _e("Vaš komentar čeka moderiranje.", 'handystore') ?></php></p>
      <?php endif; ?>

      <div class="comment-content post-content" itemprop="commentText">
          <?php comment_text() ?>
      </div>

  <?php }
  // end_el – closing HTML for comment template
  function end_el( &$output, $comment, $depth = 0, $args = array() ) { ?>
      </article>
  <?php }
}


function wpb_comment_reply_text( $link ) {
  $link = str_replace( 'Reply', 'Odgovori', $link );
  return $link;
  }
  add_filter( 'comment_reply_link', 'wpb_comment_reply_text' );